import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReposComponent } from './components/profile/repos/repos.component';
import { RepoCardComponent } from './components/profile/repo-card/repo-card.component';
import { SkeletonTextComponent } from './components/loaders/skeleton-text/skeleton-text.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ProfileLoaderComponent } from './components/loaders/profile-loader/profile-loader.component';



@NgModule({
  declarations: [
    ReposComponent,
    RepoCardComponent,
    SkeletonTextComponent,
    ProfileLoaderComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    NgxSkeletonLoaderModule
  ],
  exports: [ReposComponent,ProfileLoaderComponent]
})
export class SharedModule { }
