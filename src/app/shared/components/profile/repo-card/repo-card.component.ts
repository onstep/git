import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-repo-card',
  templateUrl: './repo-card.component.html',
  styleUrls: ['./repo-card.component.scss']
})
export class RepoCardComponent implements OnInit {

  @Input()
  repoName = '';

  @Input()
  repoDescription: any ;

  @Input()
  repoTopics: any = {};

  constructor() { }

  ngOnInit(): void {
  }
}
