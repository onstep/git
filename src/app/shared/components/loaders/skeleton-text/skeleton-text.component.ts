import { Component, OnInit } from '@angular/core';
import {LoadingService} from '../../../../core/services/loading.service';

@Component({
  selector: 'app-skeleton-text',
  templateUrl: './skeleton-text.component.html',
  styleUrls: ['./skeleton-text.component.scss']
})
export class SkeletonTextComponent implements OnInit {

  constructor(public loadingService: LoadingService) {
  }

  ngOnInit(): void {
  }

}
