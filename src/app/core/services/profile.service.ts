import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {forkJoin, Observable} from 'rxjs';
import {map, shareReplay, switchMap, tap} from 'rxjs/operators';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(
    private http: HttpClient,
    private apiService: ApiService
  ) {

  }

  getUserProfile(username: string): Observable<any> {
    return this.apiService.get('/users/' + username)
      .pipe(
        map(res => res),
        shareReplay()
      );
  }

  getUserRepos(username: string): Observable<any> {
    return this.apiService.get('/users/' + username + '/repos')
      .pipe(
        map(res => res),
        switchMap(results => {
          const urlList = results.map((details: any) => this.getTopics(details));
          return forkJoin(urlList);
        }),
        tap(joined => {
          return joined;
        }),
        shareReplay(),
      );
  }

  getTopics(repoDetails: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/vnd.github.mercy-preview+json',
      })
    };
    return this.apiService.get('/repos/' + repoDetails.full_name + '/topics', httpOptions)
      .pipe(
        map(res => {
          repoDetails.topics = res;
          return repoDetails;
        }));
  }
}
