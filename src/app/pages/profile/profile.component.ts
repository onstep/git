import { Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {
  startWith,
  tap,
  map,
} from 'rxjs/operators';
import { Observable, combineLatest} from 'rxjs';
import { faMapMarked, faExternalLinkAlt} from '@fortawesome/free-solid-svg-icons';
import { faTwitter} from '@fortawesome/free-brands-svg-icons';
import {ProfileService} from '../../core/services/profile.service';
import {LoadingService} from '../../core/services/loading.service';

interface ProfileData {
  details: any;
  repos: any;
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  data$!: Observable<ProfileData>;

  faMapMarked = faMapMarked;

  faTwitter = faTwitter;

  faExternalLinkAlt = faExternalLinkAlt;

  constructor(private profileService: ProfileService,
              private loading: LoadingService,
              private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.loadProfile();
  }

  loadProfile() {
    let username = this.route.snapshot.paramMap.get('username');
    if (!username){
       username = 'johnpapa';
    }

    const details$ = this.profileService.getUserProfile(username)
      .pipe(
        startWith(null)
      );

    const repos$ = this.profileService.getUserRepos(username)
      .pipe(
        startWith([])
      );

    this.data$ = combineLatest([details$, repos$])
      .pipe(
        map(([details, repos]) => {
          return {
            details,
            repos
          };
        }),
        tap(console.log)
      );

    this.loading.showLoaderUntilCompleted(repos$)
      .subscribe();

  }

  goToLink(url: string) {
    window.open(url, '_blank');
  }
}

